package br.com.bry.hub.sample.config.util;

import java.util.List;

public class AssinaturasDTO {

	private String cnSignatario;
	private List<String> assinaturas;

	public AssinaturasDTO(String cnSignatario, List<String> assinaturas) {
		this.cnSignatario = cnSignatario;
		this.assinaturas = assinaturas;
	}

	public AssinaturasDTO() {
	}

	public String getCnSignatario() {
		return cnSignatario;
	}

	public void setCnSignatario(String cnSignatario) {
		this.cnSignatario = cnSignatario;
	}

	public List<String> getAssinaturas() {
		return assinaturas;
	}

	public void setAssinaturas(List<String> assinaturas) {
		this.assinaturas = assinaturas;
	}
}
