package br.com.bry.hub.sample.config;

import java.util.Base64;

public class CertificateConfig {

	// If you don't have one, follow the instructions in README.md on how to obtain your personal private key.
	public static final String PRIVATE_KEY_PASSWORD = Base64.getEncoder().encodeToString("bry123456".getBytes());

}
