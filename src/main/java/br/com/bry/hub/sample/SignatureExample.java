package br.com.bry.hub.sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.bry.hub.sample.config.CertificateConfig;
import br.com.bry.hub.sample.config.ServiceConfig;
import br.com.bry.hub.sample.config.util.AssinaturasDTO;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.internal.util.IOUtils;
import io.restassured.response.Response;

public class SignatureExample {

	public static void main(String[] args) throws NoSuchAlgorithmException, IOException {

		//Obtém o documento a ser assinado
		File documento = new File(SignatureExample.class.getClassLoader().getResource("teste.txt").getFile());

		//Configura requisição
		JSONObject dados = new JSONObject();
		dados.put("signatario", "certificado_nexti");
		dados.put("algoritmoHash", "SHA256");
		dados.put("perfil", "ADRT");
		dados.put("formatoDados", "Base64");
		dados.put("formatoAssinatura", "ATTACHED");

		//Array contendo os documentos em base64. Pode ser enviado em lote,
		// basta adicionar os documentos em base64 ao array.
		JSONArray hashes = new JSONArray();
		hashes.put(Base64.getEncoder().encodeToString(IOUtils.toByteArray(new FileInputStream(documento))));
		dados.put("hashes", hashes);

		Response response = RestAssured.given().header("Content-type", ContentType.JSON)
				.header("pfx", CertificateConfig.PRIVATE_KEY_PASSWORD).body(dados.toString()).expect().when()
				.post(ServiceConfig.URL_SERVER_CMS).thenReturn();

		AssinaturasDTO assinaturasDTO = response.getBody().as(AssinaturasDTO.class);
		//Salvar as assinaturas
		for (String assinatura : assinaturasDTO.getAssinaturas()) {
			Files.write(Paths.get("assinatura.p7s"), Base64.getDecoder().decode(assinatura));
		}

	}

}
