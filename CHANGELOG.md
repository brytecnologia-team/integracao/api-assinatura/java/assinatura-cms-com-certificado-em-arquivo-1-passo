# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-02-18

### Added

- A SIGNATURE generation example. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/assinatura-cms-com-certificado-em-arquivo-1-passo/-/tags/1.0.0
